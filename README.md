# Detect multi repo change

Use git log to get the last commit that changed some sub-folder

    git log --pretty=format:'%H' --max-count=1 /path/to/folder

## Examples

Hash of last commit of the entire repo:

    git log --pretty=format:'%H' --max-count=1 .
    9bd28c01ae76d351ab37432e824a3048eb59ff63

Last commit that changed app-1 directory tree:

    git log --pretty=format:'%H' --max-count=1 app-1
    728891edfef393e570ae971f3d0a79300144a850

Last commit that changed app-2 directory tree:

    git log --pretty=format:'%H' --max-count=1 app-2
    f961bbc38c734af2478f7134a761787a37ce22aa
